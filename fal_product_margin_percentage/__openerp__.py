# -*- coding: utf-8 -*-
{
    "name": "REP-08_Product Margin Percentage",
    "version": "1.0",
    'author': 'Falinwa Hans',
    "description": """
    Module to change the view of percentage of product margin to be number.
    """,
    "depends" : ['product_margin'],
    'init_xml': [],
    'update_xml': [
        'product_margin_view.xml',
    ],
    'css': [],
    'js' : [],
    'installable': True,
    'active': False,
    'application' : False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: