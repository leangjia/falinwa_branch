# -*- coding: utf-8 -*-
{
    "name": "REP-09_Partner Company Report",
    "version": "1.0",
    'author': 'Falinwa Hans',
    "description": """
    Module to have partner company on report.
    """,
    "depends" : ['sale','purchase'],
    'init_xml': [],
    'update_xml': [

    ],
    'css': [],
    'js' : [],
    'installable': True,
    'active': False,
    'application' : False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: