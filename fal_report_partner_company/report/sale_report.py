# -*- coding: utf-8 -*-

from openerp import tools
from openerp.osv import fields, osv

class sale_report(osv.osv):
    _inherit = "sale.report"

    _columns = {
        'commercial_partner_id': fields.many2one('res.partner', 'Partner Company', help="Commercial Entity"),
    }

    _depends = {
        'account.invoice': ['section_id'],
    }
    
    def _select(self):
        return  super(sale_report, self)._select() + ", res_partner.commercial_partner_id as commercial_partner_id"

    def _from(self):
        return super(sale_report, self)._from() + """
            JOIN res_partner res_partner on s.partner_id = res_partner.id 
            JOIN res_partner commercial_partner ON res_partner.commercial_partner_id = commercial_partner.id
        """

    def _group_by(self):
        return super(sale_report, self)._group_by() + ", res_partner.commercial_partner_id"
        
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
