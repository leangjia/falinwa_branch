# -*- coding: utf-8 -*-
{
    "name": "PJC-05_Project for HPS Sale",
    "version": "1.0",
    'author': 'Falinwa Hans',
    "description": """
     
    """,
    "depends" : ['fal_project_ext', 'account'],
    'init_xml': [],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'wizard/program_generate_project_wizard_view.xml',
        'project_view.xml',
        'views/fal_hps_sale_project_view.xml',
    ],
    'update_xml': [
    ],
    'installable': True,
    'active': False,
    'application' : False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: