from datetime import datetime, date, timedelta
from lxml import etree
import time

from openerp import SUPERUSER_ID
from openerp import tools
from openerp.osv import fields, orm
from openerp.tools.translate import _

class project(orm.Model):
    _name = "project.project"
    _inherit = "project.project"
    
    def _get_fal_mold(self, cr, uid, ids, field_names, args, context=None):
        if context is None:
            context={}
        res = {}
        for project_id in self.browse(cr, uid, ids):
            #temp for global mold
            temp_total_mold = 0
            #temp for won
            temp_total_won_mold = 0
            temp_total_won_mold_no_info = []
            temp_tools_id = []
            for tools_id in project_id.tasks:
                temp_total_mold += int(tools_id.fal_mold_qty_number)
                temp_tools_id.append(tools_id.id)
                if tools_id.fal_won_mold == False:
                    temp_total_won_mold_no_info.append(tools_id.id)
                temp_total_won_mold += int(tools_id.fal_won_mold)
            res[project_id.id] = {'fal_total_mold_in_tools' : str(temp_total_mold), 'fal_total_mold_won_in_tools': False, 'color': 0}
            if len(temp_total_won_mold_no_info) == len(temp_tools_id):
                res[project_id.id]['fal_total_mold_won_in_tools'] = False
            else:
                res[project_id.id]['fal_total_mold_won_in_tools'] = str(temp_total_won_mold)

            #color definition
            #white:0, red:2, blue:7 , yellow:3, green:4, light green:5
            if res[project_id.id]['fal_total_mold_won_in_tools'] == False:                
                res[project_id.id]['color'] = 0 
            elif int(res[project_id.id]['fal_total_mold_won_in_tools']) == 0:
                res[project_id.id]['color'] = 2
            elif (int(res[project_id.id]['fal_total_mold_won_in_tools']) != 0 and int(res[project_id.id]['fal_total_mold_in_tools']) != 0) and int(res[project_id.id]['fal_total_mold_won_in_tools']) >= int(res[project_id.id]['fal_total_mold_in_tools']):
                res[project_id.id]['color'] = 5
            else:
                res[project_id.id]['color'] = 4   
        return res
        
    def _get_fal_total_mold_won_in_tools(self, cr, uid, ids, field_names, args, context=None):
        if context is None:
            context={}
        res = {}
        for project_id in self.browse(cr, uid, ids):
            temp_total_won_mold = 0
            temp_total_won_mold_no_info = []
            temp_tools_id = []
            for tools_id in project_id.tasks:
                temp_tools_id.append(tools_id.id)
                if tools_id.fal_won_mold == False:
                    temp_total_won_mold_no_info.append(tools_id.id)
                temp_total_won_mold += int(tools_id.fal_won_mold)
            if len(temp_total_won_mold_no_info) == len(temp_tools_id):
                res[project_id.id] = False
            else:
                res[project_id.id] = str(temp_total_won_mold)
        return res

    def _get_fal_color(self, cr, uid, ids, field_names, args, context=None):
        if context is None:
            context={}
        res = {}
        for project_id in self.browse(cr, uid, ids):
            #white:0, red:2, blue:7 , yellow:3, green:4
            if project_id.fal_total_mold_won_in_tools == False:                
                res[project_id.id] = 0 
            elif int(project_id.fal_total_mold_won_in_tools) == 0:
                res[project_id.id] = 2
            elif (int(project_id.fal_total_mold_won_in_tools) != 0 and int(project_id.fal_total_mold_in_tools) != 0) and int(project_id.fal_total_mold_won_in_tools) == int(project_id.fal_total_mold_in_tools):
                res[project_id.id] = 4
            else:
                res[project_id.id] = 3                
        return res

    def _get_fal_project(self, cr, uid, ids, context=None):
        result = {}
        for task in self.pool.get('project.task').browse(cr, uid, ids, context=context):
            result[task.project_id.id] = True
        return result.keys()
        
    _columns = {
        'fal_mold_qty_number': fields.char('Total Mold in Project',size=128),
        'fal_mold_qty_uom': fields.char('UOM',size=128),
        'fal_total_mold_in_tools': fields.function(_get_fal_mold, type="char", string='Total Molds in Tools', help='If it less than the total of molds for the project, it means some nfo are missing. Sales need to get more info.',
            store={
                'project.project': (lambda self, cr, uid, ids, c={}: ids, ['tasks'], 10),
                'project.task': (_get_fal_project, ['fal_mold_qty_number'], 10),                
            },
            multi='fal_mold',
        ),
        'fal_total_mold_won_in_tools': fields.function(_get_fal_mold, type="char", string='Total Molds Won in Tools', help='Do we get the order or not?',
            store={
                'project.project': (lambda self, cr, uid, ids, c={}: ids, ['tasks'], 10),
                'project.task': (_get_fal_project, ['fal_won_mold'], 10),                
            },
            multi='fal_mold',
        ),
        'color': fields.function(_get_fal_mold ,string='Color Index', type="integer",
            store={
                'project.project': (lambda self, cr, uid, ids, c={}: ids, ['tasks'], 10),
                'project.task': (_get_fal_project, ['fal_mold_qty_number','fal_won_mold'], 10), 
            },
            multi='fal_mold',
        ),        
    }
            
    _defaults = {
        'fal_mold_qty_uom' : 'Molds',
    }
    
    def onchange_moldqty(self, cr, uid, ids, fal_mold_qty_number='', fal_mold_qty_uom='', context=None):
        val = {'mold_serial_number':''}
        if fal_mold_qty_number:
            val['mold_serial_number'] += fal_mold_qty_number
        if fal_mold_qty_uom:
            val['mold_serial_number'] += ' ' + fal_mold_qty_uom
        return {'value': val}

    def fal_onchange_partner_id(self, cr, uid, ids, partner_id, name, context=None):
        partner_obj = self.pool.get('res.partner')
        val = {'name':name}
        if not name and partner_id:
            val['name'] = self.pool.get('res.partner').browse(cr, uid, partner_id).name
        if 'pricelist_id' in self.fields_get(cr, uid, context=context):
            pricelist = partner_obj.read(cr, uid, part, ['property_product_pricelist'], context=context)
            pricelist_id = pricelist.get('property_product_pricelist', False) and pricelist.get('property_product_pricelist')[0] or False
            val['pricelist_id'] = pricelist_id
        return {'value': val}
        
    def create(self, cr, uid, vals, context=None):
        account_analytic_obj = self.pool.get('account.analytic.account')
        res = super(project, self).create(cr, uid, vals, context)
        project_rec = self.browse(cr, uid, res, context=context)
        account_analytic_obj.write(cr, uid, project_rec.analytic_account_id.id, {
            'type': 'view',
        })
        return res
        
#end of project()

class task(orm.Model):
    _name = "project.task"
    _inherit = "project.task"

    def _get_fal_color(self, cr, uid, ids, field_names, args, context=None):
        if context is None:
            context={}
        res = {}
        for task_id in self.browse(cr, uid, ids):
            #white:0, red:2, blue:7 , yellow:3, green:4
            if task_id.fal_won_mold == False:                
                res[task_id.id] = 0 
            elif int(task_id.fal_won_mold) == 0:
                res[task_id.id] = 2
            elif (int(task_id.fal_won_mold) != 0 and int(task_id.fal_mold_qty_number) != 0) and int(task_id.fal_won_mold) == int(task_id.fal_mold_qty_number):
                res[task_id.id] = 5
            else:
                res[task_id.id] = 4                
        return res
    
    _columns = {
        'fal_mold_qty_number': fields.char('Total Mold Qty',size=128),
        'fal_mold_qty_uom': fields.char('UOM',size=128),
        'fal_won_mold' : fields.char('Quantity of Won', size=128),   
        'color': fields.function(_get_fal_color ,string='Color Index', type="integer", store=True),
    }
            
    _defaults = {
        'fal_mold_qty_uom' : 'Molds',
    }

    def onchange_moldqty(self, cr, uid, ids, fal_mold_qty_number='', fal_mold_qty_uom='', context=None):
        val = {'name':''}
        if fal_mold_qty_number:
            val['name'] += fal_mold_qty_number
        if fal_mold_qty_uom:
            val['name'] += ' ' + fal_mold_qty_uom
        return {'value': val}
    
    def tool_lost(self, cr, uid, ids, context=None):
        task_stage_type_obj = self.pool.get('project.task.type')
        stage_ids = task_stage_type_obj.search(cr, uid, [('name','=','Lost')])
        stage_id = False
        if stage_ids:
            stage_id = stage_ids[0]
        self.write(cr, uid, ids,{
            'fal_won_mold': '0',
            'stage_id' : stage_id,
        })
        return True
    
#end of task()

class account_analytic_account(orm.Model):
    _name = 'account.analytic.account'
    _inherit = 'account.analytic.account'
    
    _columns = {
        'fal_is_program': fields.boolean('Program'),
    }
    
    def _get_one_full_name(self, elmt, level=6):
        if level<=0:
            return '...'
        if elmt.parent_id and not elmt.type == 'template':
            parent_path = self._get_one_full_name(elmt.parent_id, level-1) + " / "
        else:
            parent_path = ''
        return parent_path + elmt.name

        
#end of account_analytic_account()
