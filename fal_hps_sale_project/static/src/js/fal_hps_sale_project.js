openerp.fal_hps_sale_project = function(openerp) {
    openerp.web_kanban.KanbanRecord.include({
        on_card_clicked: function() {
            if(this.$el.find('.oe_kanban_global_click_edit').size()>0)
                this.do_action_edit();
            else
                this.do_action_open();            
        },
    });
};