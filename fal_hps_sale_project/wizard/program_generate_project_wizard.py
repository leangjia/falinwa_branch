from openerp.osv import fields, orm
from openerp.tools.translate import _

class program_generate_project_wizard(orm.TransientModel):
    _name = "program.generate.project.wizard"
    
    _columns = {
        'program_id' : fields.many2one('project.project', 'Project'),
        'final_customer_id': fields.many2one("res.partner", "Final Customer", domain="[('is_final_customer','=',1)]", required=1)
    }
    
    def _default_program_id(self, cr, uid, context=None):        
        return context.get('active_id', False)
    
    _defaults = {
        'program_id' : _default_program_id,
    }
    
    def generate_project(self, cr, uid, ids, context):
        project_obj = self.pool.get('project.project')
        wizard_data = self.browse(cr, uid, ids)[0]
        new_project_id = project_obj.create(cr, uid, {
            'name' : wizard_data.final_customer_id.name,
            'parent_id' : wizard_data.program_id.analytic_account_id.id,
            'partner_id' : wizard_data.final_customer_id.id,
            'project_opportunities' : 1,
            'use_tasks' : 1,
        })
        mod_obj = self.pool.get('ir.model.data')
        model_data_ids = mod_obj.search(cr, uid,[('model', '=', 'ir.ui.view'), ('name', '=', 'edit_project_fal_pext')], context=context)
        resource_id = mod_obj.read(cr, uid, model_data_ids, fields=['res_id'], context=context)[0]['res_id']        
        return {
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'project.project',
            'views': [(resource_id,'form')],
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'res_id': new_project_id,
        }
        
#end of program_generate_project_wizard()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
