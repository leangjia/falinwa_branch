# -*- coding: utf-8 -*-
from openerp.osv import fields, orm
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
import time
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, DATETIME_FORMATS_MAP, float_compare
from openerp import SUPERUSER_ID

class mrp_production(orm.Model):
    _name = 'mrp.production'    
    _inherit = 'mrp.production'      

    def _get_one_fal_production_root_serie_name_id(self, production_id):
        root_serie_name_id = production_id.fal_serie_name_id
        if production_id.fal_parent_mo_id:
            root_serie_name_id = self._get_one_fal_production_root_serie_name_id(production_id.fal_parent_mo_id)
        return root_serie_name_id
            
    def _get_fal_production_root_serie_name_id(self, cr, uid, ids, field_name, arg, context=None):
        res = {}
        for production_id in self.browse(cr, uid, ids, context=context):
            root_serie_name_id = self._get_one_fal_production_root_serie_name_id(production_id)
            if root_serie_name_id:
                res[production_id.id] = root_serie_name_id.id
            else:
                res[production_id.id] = False
        return res
        
    def _get_one_fal_production_root(self, production_id):
        root_production_id = production_id
        if production_id.fal_parent_mo_id:
            root_production_id = self._get_one_fal_production_root(production_id.fal_parent_mo_id)
        return root_production_id

    def _get_display_product_name(self, production_id):
        display_product_name = str(production_id.fal_serie_name_id.name)
        if production_id.fal_bore_diameter_id:
            display_product_name += '-' + production_id.fal_bore_diameter_id.name
        if production_id.fal_rod_diameter_id:
            display_product_name += '-' + production_id.fal_rod_diameter_id.name
        if production_id.fal_mounting_id:
            display_product_name += '-' + production_id.fal_mounting_id.code
        display_product_name += '-' + str(production_id.fal_stroke)
        return display_product_name

    def _compute_fal_product_display_name(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        for production_id in self.browse(cr, uid, ids, context=context):
            if production_id.product_id.categ_id.isfal_finished_product:
                res[production_id.id] = self._get_display_product_name(production_id)
            else:
                if production_id.product_id.default_code:
                    res[production_id.id] = production_id.product_id.default_code[2:4] + '-' + self._get_display_product_name(self._get_one_fal_production_root(production_id))                    
                else:    
                    res[production_id.id] = self._get_display_product_name(self._get_one_fal_production_root(production_id))                                    
        return res
        
    def _compute_fal_date_planned(self, cr, uid, ids, field, arg, context=None):
        res = dict.fromkeys(ids, False)
        for production_id in self.browse(cr, uid, ids, context=context):
                res[production_id.id] = production_id.date_planned
        return res
        
    _columns = {
        'fal_production_root_serie_name_id' : fields.function(_get_fal_production_root_serie_name_id, type='many2one', relation='fal.serie.name', string='Root Production Order Serie Name', store=False),
        'date_planned': fields.datetime('Scheduled Date', required=True, select=1, readonly=True, states={'draft': [('readonly', False)]}, copy=False),
        'fal_date_planned': fields.function(_compute_fal_date_planned, string='Schedule Date', type="date"),
        'fal_product_display_name' : fields.function(_compute_fal_product_display_name, string='Product Display Name', type="char"),

    }

    _defaults = {
        'date_planned': fields.date.today(),
    }
    
#end of mrp_production()