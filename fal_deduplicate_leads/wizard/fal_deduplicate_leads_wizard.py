#!/usr/bin/env python
from __future__ import absolute_import
from email.utils import parseaddr
import functools
import htmlentitydefs
import itertools
import logging
import operator
import psycopg2
import re
from ast import literal_eval
from openerp.tools import mute_logger


import openerp
from openerp.osv import osv, orm
from openerp.osv import fields
from openerp.osv.orm import browse_record
from openerp.tools.translate import _
from openerp.addons.crm.validate_email import validate_email
pattern = re.compile("&(\w+?);")

_logger = logging.getLogger('base.leads.merge')


# http://www.php2python.com/wiki/function.html-entity-decode/
def html_entity_decode_char(m, defs=htmlentitydefs.entitydefs):
    try:
        return defs[m.group(1)]
    except KeyError:
        return m.group(0)


def html_entity_decode(string):
    return pattern.sub(html_entity_decode_char, string)


def sanitize_email(email):
    assert isinstance(email, basestring) and email

    result = re.subn(r';|/|:', ',',
                     html_entity_decode(email or ''))[0].split(',')

    emails = [parseaddr(email)[1]
              for item in result
              for email in item.split()]

    return [email.lower()
            for email in emails
            if validate_email(email)]


def is_integer_list(ids):
    return all(isinstance(i, (int, long)) for i in ids)
    
class FalMergeLeadsLine(osv.TransientModel):
    _name = 'fal.leads.merge.line'

    _columns = {
        'wizard_id': fields.many2one('base.leads.merge.automatic.wizard',
                                     'Wizard'),
        'min_id': fields.integer('MinID'),
        'aggr_ids': fields.char('Ids', required=True),
    }

    _order = 'min_id asc'


class FalMergeLeadsAutomatic(osv.TransientModel):
    """
        The idea behind this wizard is to create a list of potential leads to
        merge. We use two objects, the first one is the wizard for the end-user.
        And the second will contain the leads list to merge.
    """
    _name = 'fal.leads.merge.automatic.wizard'

    _columns = {
        # Group by
        'group_by_email_from': fields.boolean('Email'),
        'group_by_contact_name': fields.boolean('Contact Name'),
        'group_by_partner_name': fields.boolean('Company Name'),
        'group_by_name': fields.boolean('Subject'),
        'state': fields.selection([('option', 'Option'),
                                   ('selection', 'Selection'),
                                   ('finished', 'Finished')],
                                  'State',
                                  readonly=True,
                                  required=True),
        'number_group': fields.integer("Group of Leads", readonly=True),
        'current_line_id': fields.many2one('fal.leads.merge.line', 'Current Line'),
        'line_ids': fields.one2many('fal.leads.merge.line', 'wizard_id', 'Lines'),
        'leads_ids': fields.many2many('crm.lead', string='Leads'),
        'dst_leads_id': fields.many2one('crm.lead', string='Destination Leads'),
        'maximum_group': fields.integer("Maximum of Group of Leads"),
    }

    def default_get(self, cr, uid, fields, context=None):
        if context is None:
            context = {}
        res = super(FalMergeLeadsAutomatic, self).default_get(cr, uid, fields, context)
        if context.get('active_model') == 'crm.lead' and context.get('active_ids'):
            leads_ids = context['active_ids']
            res['state'] = 'selection'
            res['leads_ids'] = leads_ids
            res['dst_leads_id'] = self._get_ordered_leads(cr, uid, leads_ids, context=context)[-1].id
        return res

    _defaults = {
        'state': 'option'
    }

    def get_fk_on(self, cr, table):
        q = """  SELECT cl1.relname as table,
                        att1.attname as column
                   FROM pg_constraint as con, pg_class as cl1, pg_class as cl2,
                        pg_attribute as att1, pg_attribute as att2
                  WHERE con.conrelid = cl1.oid
                    AND con.confrelid = cl2.oid
                    AND array_lower(con.conkey, 1) = 1
                    AND con.conkey[1] = att1.attnum
                    AND att1.attrelid = cl1.oid
                    AND cl2.relname = %s
                    AND att2.attname = 'id'
                    AND array_lower(con.confkey, 1) = 1
                    AND con.confkey[1] = att2.attnum
                    AND att2.attrelid = cl2.oid
                    AND con.contype = 'f'
        """
        return cr.execute(q, (table,))

    @mute_logger('openerp.osv.expression', 'openerp.models')
    def _merge(self, cr, uid, leads_ids, dst_leads=None, context=None):
        proxy = self.pool.get('crm.lead')

        leads_ids = proxy.exists(cr, uid, list(leads_ids), context=context)
        if len(leads_ids) < 2:
            return

        if len(leads_ids) > 3:
            raise osv.except_osv(_('Error'), _("For safety reasons, you cannot merge more than 3 leads together. You can re-open the wizard several times if needed."))

        if openerp.SUPERUSER_ID != uid and len(set(leads.email_from for leads in proxy.browse(cr, uid, leads_ids, context=context))) > 1:
            raise osv.except_osv(_('Error'), _("All leads must have the same email. Only the Administrator can merge contacts with different emails."))

        if dst_leads and dst_leads.id in leads_ids:
            src_leadss = proxy.browse(cr, uid, [id for id in leads_ids if id != dst_leads.id], context=context)
        else:
            ordered_leadss = self._get_ordered_leads(cr, uid, leads_ids, context)
            dst_leads = ordered_leadss[-1]
            src_leadss = ordered_leadss[:-1]
        _logger.info("dst_leads: %s", dst_leads.id)

        if openerp.SUPERUSER_ID != uid and self._model_is_installed(cr, uid, 'account.move.line', context=context) and \
                self.pool.get('account.move.line').search(cr, openerp.SUPERUSER_ID, [('leads_id', 'in', [leads.id for leads in src_leadss])], context=context):
            raise osv.except_osv(_('Error'), _("Only the destination contact may be linked to existing Journal Items. Please ask the Administrator if you need to merge several contacts linked to existing Journal Items."))

        call_it = lambda function: function(cr, uid, src_leadss, dst_leads,
                                            context=context)

        #call_it(self._update_foreign_keys)
        #call_it(self._update_reference_fields)
        #call_it(self._update_values)

        _logger.info('(uid = %s) merged the leadss %r with %s', uid, list(map(operator.attrgetter('id'), src_leadss)), dst_leads.id)
        dst_leads.message_post(body='%s %s'%(_("Merged with the following leads:"), ", ".join('%s<%s>(ID %s)' % (p.contact_name, p.email_from or 'n/a', p.id) for p in src_leadss)))
        
        for leads in src_leadss:
            leads.unlink()

    def clean_emails(self, cr, uid, context=None):
        """
        Clean the email address of the leads, if there is an email field with
        a mimum of two addresses, the system will create a new leads, with the
        information of the previous one and will copy the new cleaned email into
        the email field.
        """
        context = dict(context or {})

        proxy_model = self.pool['ir.model.fields']
        field_ids = proxy_model.search(cr, uid, [('model', '=', 'crm.lead'),
                                                 ('ttype', 'like', '%2many')],
                                       context=context)
        fields = proxy_model.read(cr, uid, field_ids, context=context)
        reset_fields = dict((field['contact_name'], []) for field in fields)

        proxy_leads = self.pool['crm.lead']
        context['active_test'] = False
        ids = proxy_leads.search(cr, uid, [], context=context)

        fields = ['contact_name', 'email_from', 'name', 'partner_name']
        leadss = proxy_leads.read(cr, uid, ids, fields, context=context)

        leadss.sort(key=operator.itemgetter('id'))
        leadss_len = len(leadss)

        _logger.info('leads_len: %r', leadss_len)

        for idx, leads in enumerate(leadss):
            if not leads['email_from']:
                continue

            percent = (idx / float(leadss_len)) * 100.0
            _logger.info('idx: %r', idx)
            _logger.info('percent: %r', percent)
            try:
                emails = sanitize_email(leads['email_from'])
                head, tail = emails[:1], emails[1:]
                email = head[0] if head else False

                proxy_leads.write(cr, uid, [leads['id']],
                                    {'email_from': email}, context=context)

                for email in tail:
                    values = dict(reset_fields, email_from=email)
                    proxy_leads.copy(cr, uid, leads['id'], values,
                                       context=context)

            except Exception:
                _logger.exception("There is a problem with this leads: %r", leads)
                raise
        return True

    def close_cb(self, cr, uid, ids, context=None):
        return {'type': 'ir.actions.act_window_close'}

    def _generate_query(self, fields, maximum_group=100):
        sql_fields = []
        for field in fields:
            if field in ['email_from', 'contact_name', 'name', 'partner_name']:
                sql_fields.append('lower(%s)' % field)
            else:
                sql_fields.append(field)

        group_fields = ', '.join(sql_fields)

        filters = []
        for field in fields:
            if field in ['email_from', 'contact_name', 'name', 'partner_name']:
                filters.append((field, 'IS NOT', 'NULL'))

        criteria = ' AND '.join('%s %s %s' % (field, operator, value)
                                for field, operator, value in filters)

        text = [
            "SELECT min(id), array_agg(id)",
            "FROM crm_lead",
        ]

        if criteria:
            text.append('WHERE %s' % criteria)

        text.extend([
            "GROUP BY %s" % group_fields,
            "HAVING COUNT(*) >= 2",
            "ORDER BY min(id)",
        ])

        if maximum_group:
            text.extend([
                "LIMIT %s" % maximum_group,
            ])

        return ' '.join(text)

    def _compute_selected_groupby(self, this):
        group_by_str = 'group_by_'
        group_by_len = len(group_by_str)

        fields = [
            key[group_by_len:]
            for key in self._columns.keys()
            if key.startswith(group_by_str)
        ]

        groups = [
            field
            for field in fields
            if getattr(this, '%s%s' % (group_by_str, field), False)
        ]

        if not groups:
            raise osv.except_osv(_('Error'),
                                 _("You have to specify a filter for your selection"))

        return groups

    def next_cb(self, cr, uid, ids, context=None):
        """
        Don't compute any thing
        """
        context = dict(context or {}, active_test=False)
        this = self.browse(cr, uid, ids[0], context=context)
        if this.current_line_id:
            this.current_line_id.unlink()
        return self._next_screen(cr, uid, this, context)

    def _get_ordered_leads(self, cr, uid, leads_ids, context=None):
        leadss = self.pool.get('crm.lead').browse(cr, uid, list(leads_ids), context=context)
        ordered_leadss = sorted(sorted(leadss,
                            key=operator.attrgetter('create_date'), reverse=True),
                                key=operator.attrgetter('active'), reverse=True)
        return ordered_leadss

    def _next_screen(self, cr, uid, this, context=None):
        this.refresh()
        values = {}
        if this.line_ids:
            # in this case, we try to find the next record.
            current_line = this.line_ids[0]
            current_leads_ids = literal_eval(current_line.aggr_ids)
            values.update({
                'current_line_id': current_line.id,
                'leads_ids': [(6, 0, current_leads_ids)],
                'dst_leads_id': self._get_ordered_leads(cr, uid, current_leads_ids, context)[-1].id,
                'state': 'selection',
            })
        else:
            values.update({
                'current_line_id': False,
                'leads_ids': [],
                'state': 'finished',
            })

        this.write(values)

        return {
            'type': 'ir.actions.act_window',
            'res_model': this._name,
            'res_id': this.id,
            'view_mode': 'form',
            'target': 'new',
        }

    def _model_is_installed(self, cr, uid, model, context=None):
        proxy = self.pool.get('ir.model')
        domain = [('model', '=', model)]
        return proxy.search_count(cr, uid, domain, context=context) > 0

    def _leads_use_in(self, cr, uid, aggr_ids, models, context=None):
        """
        Check if there is no occurence of this group of leads in the selected
        model
        """
        for model, field in models.iteritems():
            proxy = self.pool.get(model)
            domain = [(field, 'in', aggr_ids)]
            if proxy.search_count(cr, uid, domain, context=context):
                return True
        return False

    def compute_models(self, cr, uid, ids, context=None):
        """
        Compute the different models needed by the system if you want to exclude
        some leadss.
        """
        assert is_integer_list(ids)

        this = self.browse(cr, uid, ids[0], context=context)

        models = {}

        return models

    def _process_query(self, cr, uid, ids, query, context=None):
        """
        Execute the select request and write the result in this wizard
        """
        proxy = self.pool.get('fal.leads.merge.line')
        this = self.browse(cr, uid, ids[0], context=context)
        models = self.compute_models(cr, uid, ids, context=context)
        cr.execute(query)

        counter = 0
        for min_id, aggr_ids in cr.fetchall():
            if models and self._leads_use_in(cr, uid, aggr_ids, models, context=context):
                continue
            values = {
                'wizard_id': this.id,
                'min_id': min_id,
                'aggr_ids': aggr_ids,
            }

            proxy.create(cr, uid, values, context=context)
            counter += 1

        values = {
            'state': 'selection',
            'number_group': counter,
        }

        this.write(values)

        _logger.info("counter: %s", counter)

    def start_process_cb(self, cr, uid, ids, context=None):
        """
        Start the process.
        * Compute the selected groups (with duplication)
        * If the user has selected the 'exclude_XXX' fields, avoid the leadss.
        """
        assert is_integer_list(ids)

        context = dict(context or {}, active_test=False)
        this = self.browse(cr, uid, ids[0], context=context)
        groups = self._compute_selected_groupby(this)
        query = self._generate_query(groups, this.maximum_group)
        self._process_query(cr, uid, ids, query, context=context)

        return self._next_screen(cr, uid, this, context)

    def automatic_process_cb(self, cr, uid, ids, context=None):
        assert is_integer_list(ids)
        this = self.browse(cr, uid, ids[0], context=context)
        this.start_process_cb()
        this.refresh()

        for line in this.line_ids:
            leads_ids = literal_eval(line.aggr_ids)
            self._merge(cr, uid, leads_ids, context=context)
            line.unlink()
            cr.commit()

        this.write({'state': 'finished'})
        return {
            'type': 'ir.actions.act_window',
            'res_model': this._name,
            'res_id': this.id,
            'view_mode': 'form',
            'target': 'new',
        }

    def parent_migration_process_cb(self, cr, uid, ids, context=None):
        assert is_integer_list(ids)

        context = dict(context or {}, active_test=False)
        this = self.browse(cr, uid, ids[0], context=context)

        query = """
            SELECT
                min(p1.id),
                array_agg(DISTINCT p1.id)
            FROM
                res_leads as p1
            INNER join
                res_leads as p2
            ON
                p1.email_from = p2.email_from AND
                p1.name = p2.contact_name AND
                (p1.parent_id = p2.id OR p1.id = p2.parent_id)
            WHERE
                p2.id IS NOT NULL
            GROUP BY
                p1.email_from,
                p1.contact_name,
                CASE WHEN p1.parent_id = p2.id THEN p2.id
                    ELSE p1.id
                END
            HAVING COUNT(*) >= 2
            ORDER BY
                min(p1.id)
        """

        self._process_query(cr, uid, ids, query, context=context)

        for line in this.line_ids:
            leads_ids = literal_eval(line.aggr_ids)
            self._merge(cr, uid, leads_ids, context=context)
            line.unlink()
            cr.commit()

        this.write({'state': 'finished'})

        cr.execute("""
            UPDATE
                res_leads
            SET
                is_company = NULL,
                parent_id = NULL
            WHERE
                parent_id = id
        """)

        return {
            'type': 'ir.actions.act_window',
            'res_model': this._name,
            'res_id': this.id,
            'view_mode': 'form',
            'target': 'new',
        }

    def update_all_process_cb(self, cr, uid, ids, context=None):
        assert is_integer_list(ids)

        # WITH RECURSIVE cycle(id, parent_id) AS (
        #     SELECT id, parent_id FROM res_leads
        #   UNION
        #     SELECT  cycle.id, res_leads.parent_id
        #     FROM    res_leads, cycle
        #     WHERE   res_leads.id = cycle.parent_id AND
        #             cycle.id != cycle.parent_id
        # )
        # UPDATE  res_leads
        # SET     parent_id = NULL
        # WHERE   id in (SELECT id FROM cycle WHERE id = parent_id);

        this = self.browse(cr, uid, ids[0], context=context)

        self.parent_migration_process_cb(cr, uid, ids, context=None)

        list_merge = [
            {'group_by_vat': True, 'group_by_email': True, 'group_by_name': True},
            # {'group_by_name': True, 'group_by_is_company': True, 'group_by_parent_id': True},
            # {'group_by_email': True, 'group_by_is_company': True, 'group_by_parent_id': True},
            # {'group_by_name': True, 'group_by_vat': True, 'group_by_is_company': True, 'exclude_journal_item': True},
            # {'group_by_email': True, 'group_by_vat': True, 'group_by_is_company': True, 'exclude_journal_item': True},
            # {'group_by_email': True, 'group_by_is_company': True, 'exclude_contact': True, 'exclude_journal_item': True},
            # {'group_by_name': True, 'group_by_is_company': True, 'exclude_contact': True, 'exclude_journal_item': True}
        ]

        for merge_value in list_merge:
            id = self.create(cr, uid, merge_value, context=context)
            self.automatic_process_cb(cr, uid, [id], context=context)

        return self._next_screen(cr, uid, this, context)

    def merge_cb(self, cr, uid, ids, context=None):
        assert is_integer_list(ids)

        context = dict(context or {}, active_test=False)
        this = self.browse(cr, uid, ids[0], context=context)

        leads_ids = set(map(int, this.leads_ids))
        if not leads_ids:
            this.write({'state': 'finished'})
            return {
                'type': 'ir.actions.act_window',
                'res_model': this._name,
                'res_id': this.id,
                'view_mode': 'form',
                'target': 'new',
            }

        self._merge(cr, uid, leads_ids, this.dst_leads_id, context=context)

        if this.current_line_id:
            this.current_line_id.unlink()

        return self._next_screen(cr, uid, this, context)

