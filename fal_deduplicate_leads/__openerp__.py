# -*- coding: utf-8 -*-
{
    "name": "CRM-05_Deduplicate leads",
    "version": "1.0",
    'author': 'Falinwa Hans',
    "description": """
    Module to give feature to deduplidate lead.
    """,
    "depends" : ['base','crm'],
    'init_xml': [],
    'data': [
    ],
    'update_xml': [
        'wizard/fal_deduplicate_leads_wizard_view.xml',
    ],
    'css': [],
    'js' : [
    ],
    'qweb': [],
    'installable': True,
    'active': False,
    'application' : False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: