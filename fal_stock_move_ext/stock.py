# -*- coding: utf-8 -*-
from openerp import fields, models, api
from openerp.tools.translate import _

class stock_move(models.Model):
    _name = "stock.move"
    _inherit = "stock.move"
        
    date = fields.Datetime(string="Move Date")
    
#end of stock_move()