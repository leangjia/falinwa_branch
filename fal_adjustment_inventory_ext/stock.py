# -*- coding: utf-8 -*-
from openerp import fields, models, api
from openerp.tools.translate import _

class stock_inventory(models.Model):
    _name = "stock.inventory"
    _inherit = "stock.inventory"
    
    @api.model
    def _get_available_filters(self):
        res = super(stock_inventory, self)._get_available_filters()
        res.append(('by categories', _('By Product Categories')))
        res.append(('by shelf', _('By Shelf')))
        return res
    
    #fields start here
    filter = fields.Selection(_get_available_filters, 'Inventory of', required=True,
                                   help="If you do an entire inventory, you can choose 'All Products' and it will prefill the inventory with the current stock.  If you only do some products  "\
                                      "(e.g. Cycle Counting) you can choose 'Manual Selection of Products' and the system won't propose anything.  You can also let the "\
                                      "system propose for a single product / lot /... ")

    fal_internal_category_id = fields.Many2one('product.category','Internal Category', readonly=True, states={'draft': [('readonly', False)]})
    fal_shelf = fields.Char('Shelf', readonly=True, states={'draft': [('readonly', False)]}, help="You can do more than 1 shelf, example: shelf1;shelf2;shelf3")    
    #end here
    
    @api.model
    def _get_inventory_lines_bycategory(self, inventory):
        product_obj = self.env['product.product']
        quant_obj = self.env["stock.quant"]
        vals = []
        product_ids = product_obj.search([('categ_id','child_of', inventory.fal_internal_category_id.id)])
        for product_id in product_ids:
            dom = [('company_id', '=', inventory.company_id.id), ('location_id', '=', inventory.location_id.id),
                            ('product_id','=', product_id.id)]
            quants = quant_obj.search(dom)
            tot_qty = sum([x.qty for x in quants])
            vals.append({
                'inventory_id': inventory.id,
                'location_id': inventory.location_id.id,
                'product_id': product_id.id,
                'product_uom_id': product_id.uom_id.id,
                'product_qty': tot_qty,
            })
        return vals

    @api.model
    def _get_inventory_lines_byself(self, inventory):
        product_obj = self.env['product.product']
        quant_obj = self.env["stock.quant"]
        vals = []
        shelfs = inventory.fal_shelf.replace(" ", "").split(';')
        product_ids = product_obj.search([('loc_rack','in', shelfs)])
        for product_id in product_ids:
            dom = [('company_id', '=', inventory.company_id.id), ('location_id', '=', inventory.location_id.id),
                            ('product_id','=', product_id.id)]
            quants = quant_obj.search(dom)
            tot_qty = sum([x.qty for x in quants])
            vals.append({
                'inventory_id': inventory.id,
                'location_id': inventory.location_id.id,
                'product_id': product_id.id,
                'product_uom_id': product_id.uom_id.id,
                'product_qty': tot_qty,
            })
        return vals
        
    @api.multi
    def prepare_inventory(self):
        product_obj = self.env['product.product']
        inventory_line_obj = self.env['stock.inventory.line']
        for inventory in self:
            line_ids = [line.id for line in inventory.line_ids]
            if not line_ids and inventory.filter == 'by categories':
                fal_vals = self._get_inventory_lines_bycategory(inventory)
                for fal_product in fal_vals:                
                    inventory_line_obj.create(fal_product)
            if not line_ids and inventory.filter == 'by shelf':
                fal_vals = self._get_inventory_lines_byself(inventory)
                for fal_product in fal_vals:                
                    inventory_line_obj.create(fal_product)                
        return super(stock_inventory, self).prepare_inventory()  
                
#end of stock_inventory()


class stock_inventory_line(models.Model):
    _name = "stock.inventory.line"
    _inherit = "stock.inventory.line"
    
    @api.multi
    @api.depends('theoretical_qty', 'product_qty')
    def _compute_fal_is_adjustment(self):
        for inventory_line in self:
            inventory_line.fal_is_adjustment = inventory_line.theoretical_qty != inventory_line.product_qty
                
    #fields start here
    fal_explanation = fields.Text("Explanation")
    fal_is_adjustment = fields.Boolean(compute=_compute_fal_is_adjustment, string="Is Adjustment")
    #end here
    
#end of stock_inventory_line()