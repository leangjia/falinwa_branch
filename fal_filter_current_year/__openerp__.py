# -*- coding: utf-8 -*-
{
    "name": "GEN-46_Filter By current Year",
    "version": "1.0",
    'author': 'Falinwa Hans',
    "description": """
    Module to filter by current Year
    
    """,
    "depends" : ['sale', 'purchase', 'account', 'account_voucher'],
    'init_xml': [],
    'data': [
    ],
    'update_xml': [
        'general_view.xml',
    ],
    'css': [],
    'installable': True,
    'active': False,
    'application' : False,
    'js': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: